// 2018 YZU Linear algebra read bitmap rgb as pixel
// https://msdn.microsoft.com/zh-tw/library/windows/desktop/dd183376(v=vs.85).aspx

#include <iostream>
#include <fstream>

using std::cout;
using std::endl;
using std::ofstream;
using std::ifstream;

#pragma pack(1)
#pragma once

typedef int LONG;
typedef unsigned short WORD;
typedef unsigned int DWORD;

typedef struct tagBITMAPFILEHEADER
{
	WORD bfType;
	DWORD bfSize;
	WORD bfReserved1;
	WORD bfReserved2;
	DWORD bfOffBits;
} BITMAPFILEHEADER, *PBITMAPFILEHEADER;

typedef struct tagBITMAPINFOHEADER
{
	DWORD biSize;
	LONG biWidth;
	LONG biHeight;
	WORD biPlanes;
	WORD biBitCount;
	DWORD biCompression;
	DWORD biSizeImage;
	LONG biXPelsPerMeter;
	LONG biYPelsPerMeter;
	DWORD biClrUsed;
	DWORD biClrImportant;
} BITMAPINFOHEADER, *PBITMAPINFOHEADER;

unsigned char** reds;
unsigned char** greens;
unsigned char** blues;
int rows;
int cols;

void RGB_Allocate(unsigned char**& dude)
{
	dude = new unsigned char*[rows];
	for (int i = 0; i < rows; i++)
	{
		dude[i] = new unsigned char[cols];

	}
}

bool FillAndAllocate(char*& buffer, const char* Picture, int& rows, int& cols, int& BufferSize)   //Returns 1 if executed sucessfully, 0 if not sucessfull
{
	std::ifstream file(Picture);

	if (file)
	{
		file.seekg(0, std::ios::end);
		std::streampos length = file.tellg();
		file.seekg(0, std::ios::beg);

		buffer = new char[length];
		file.read(&buffer[0], length);

		PBITMAPFILEHEADER file_header;
		PBITMAPINFOHEADER info_header;

		file_header = (PBITMAPFILEHEADER)(&buffer[0]);
		info_header = (PBITMAPINFOHEADER)(&buffer[0] + sizeof(BITMAPFILEHEADER));
		rows = info_header->biHeight;
		cols = info_header->biWidth;
		BufferSize = file_header->bfSize;
		return 1;
	}
	else
	{
		cout << "File" << Picture << " don't Exist!" << endl;
		return 0;
	}
}

void GetPixlesFromBMP24(unsigned char** reds, unsigned char** greens, unsigned char** blues, int end, int rows, int cols, char* FileReadBuffer)   // end is BufferSize (total size of file)
{
	int count = 1;
	int extra = cols % 4; // The nubmer of bytes in a row (cols) will be a multiple of 4.
	for (int i = 0; i < rows; i++)
	{
		count += extra;
		for (int j = cols - 1; j >= 0; j--)
			for (int k = 0; k < 3; k++)
			{
				switch (k)
				{
				case 0:
					reds[i][j] = FileReadBuffer[end - count++];

					break;
				case 1:
					greens[i][j] = FileReadBuffer[end - count++];
					break;
				case 2:
					blues[i][j] = FileReadBuffer[end - count++];
					break;
				}
			}
	}

}

int main(int args, char** argv)
{
	char* FileBuffer;
	int BufferSize;

#define Picture argv[1]
	if (!FillAndAllocate(FileBuffer, Picture, rows, cols, BufferSize))
	{
		cout << "File read error" << endl;
		return 0;
	}
	cout << "Rows: " << rows << " Cols: " << cols << endl;

	RGB_Allocate(reds);
	RGB_Allocate(greens);
	RGB_Allocate(blues);
	GetPixlesFromBMP24(reds, greens, blues, BufferSize, rows, cols, FileBuffer);

	cout << "B G R" << '\n';
	for (int i = 0; i < rows; i++)
	{
		for (int j = cols - 1; j >= 0; j--)
		{
			cout << int(reds[i][j]) << " ";
			cout << int(greens[i][j]) << " ";
			cout << int(blues[i][j]) << " ";
		}
		cout << '\n';
	}
	system("pause");
	return 1;
}
