#include <iostream>
#include <vector>
using namespace std;

void init_2Dvector(vector<vector<double> >& vec, int x_dim, int y_dim)
{
	vec.resize(x_dim);
	for (int i = 0; i < vec.size(); i++)
		vec[i].resize(y_dim);
}

vector<vector<double> > Matricx_multiplication(vector<vector<double> >a_matrix, vector<vector<double> > b_matrix, vector<vector<double> > out_matrix, int matrix_size)
{
	init_2Dvector(out_matrix, matrix_size, matrix_size);
	for (int i = 0; i < matrix_size; i++)
	{
		for (int j = 0; j < matrix_size; j++)
		{
			out_matrix[i][j] = 0;
			for (int k = 0; k < matrix_size; k++)
			{
				out_matrix[i][j] += a_matrix[i][k] * b_matrix[k][j];
			}
		}
	}
	return out_matrix;
}


double finding_determinant(vector<vector<double> > mat)
{
	double determinant = 0;
	for (int i = 0; i < mat.size(); i++)
		determinant = determinant + (mat[0][i] * (mat[1][(i + 1) % mat.size()] * mat[2][(i + 2) % 3] - mat[1][(i + 2) % 3] * mat[2][(i + 1) % 3]));
	return determinant;
}

vector<vector<double> > return_inv_matrix(vector<vector<double> > mat)
{

	double determinant = 0;

	vector<vector<double> > inv_mat;

	inv_mat.resize(3);
	for (int i = 0; i < mat.size(); ++i)
		inv_mat[i].resize(3);

	printf("\nGiven matrix is:");
	for (int i = 0; i < mat.size(); i++)
	{
		cout << "\n";
		for (int j = 0; j < mat.size(); j++)
			cout << mat[i][j] << "\t";
	}

	//finding determinant
	determinant = finding_determinant(mat);
	cout << "\n\ndeterminant: " << determinant;

	cout << "\n\nInverse of matrix is: \n";
	for (int i = 0; i < mat.size(); i++)
	{
		for (int j = 0; j < mat.size(); j++)
			inv_mat[i][j] = ((mat[(j + 1) % 3][(i + 1) % 3] * mat[(j + 2) % 3][(i + 2) % 3]) - (mat[(j + 1) % 3][(i + 2) % 3] * mat[(j + 2) % 3][(i + 1) % 3])) / determinant;
	}
	return (inv_mat);
}

int main(int argc, char *argv[])
{
	vector<vector<double> > mat
	{
		{ 1, 2, -1 },    /*  initializers for row indexed by 0 */
		{ 1, 4, 1 },    /*  initializers for row indexed by 1 */
		{ 2, -1, 2 }   /*  initializers for row indexed by 2 */
	};

	//////////Matricx_inverse_result////////////////

	vector<vector<double> > inv_mat = return_inv_matrix(mat);

	for (int i = 0; i < mat.size(); i++)
	{
		for (int j = 0; j < mat.size(); j++)
			cout << inv_mat[i][j] << "\t";
		cout << "\n";
	}
	//////////Matricx_inverse_result////////////////


	//////////Matricx_multiplication_result////////////////
	vector<vector<double> > Matricx_multiplication_result;

	cout << '\n' << "Matricx_multiplication_result" << '\n';

	Matricx_multiplication_result = Matricx_multiplication(inv_mat, inv_mat, Matricx_multiplication_result, 3);

	for (int i = 0; i < Matricx_multiplication_result.size(); i++)
	{
		for (int j = 0; j < Matricx_multiplication_result.size(); j++)
			cout << Matricx_multiplication_result[i][j] << '\t';
		cout << '\n';
	}
	//////////Matricx_multiplication_result////////////////

	system("pause");

}
